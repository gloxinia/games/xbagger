#nullable enable

using System.Collections.Generic;
using Gloxinia.Xbagger.Stock;
using Gloxinia.Xbagger.UI;
using UnityEngine;

namespace Gloxinia.Xbagger.Core
{
    /// <summary>
    /// ゲームの全体的な流れを管理するクラス
    /// </summary>
    internal class GameDirector
    {
        #region Internal API

        internal int WeekLength { get; private set; } = 0;

        internal StockExchange StockExchange { get; private set; } = null!;

        internal TimeKeeper TimeKeeper { get; private set; } = null!;

        internal Dictionary<string, StockScript> UIStocks { get; private set; } = new();

        internal GameDirector(int limitWeeks)
        {
            StockExchange = new StockExchange(limitWeeks);
            TimeKeeper = new TimeKeeper(limitWeeks);
        }

        internal void RegisterUIScreen(ScreenScript screenScript)
        {
            m_screenScript = screenScript;
        }

        internal void RegisterUIMenu(MenuScript menuScript)
        {
            m_menuScript = menuScript;
        }

        internal void RegisterUIStocks(List<StockProfile> stockProfileList, List<StockScript> m_stockScriptList)
        {
            UIStocks = new();
            var i = 0;

            foreach (var stockProfile in stockProfileList)
            {
                var stockScript = m_stockScriptList[i];

                // 辞書に StockScript を設定する
                UIStocks[stockProfile.TickerSymbol] = stockScript;

                // StockScript の更新されない値を設定する
                stockScript.TickerSymbol = stockProfile.TickerSymbol;
                stockScript.Name.text = stockProfile.TickerSymbol;
                stockScript.Icon.sprite = stockProfile.CompanyIcon;

                i++;
            }
        }

        internal void RegisterBGColors(Color selectedBGColor, Color unselectedBGColor)
        {
            m_selectedBGColor = selectedBGColor;
            m_unselectedBGColor = unselectedBGColor;
        }

        internal void Awake()
        {
            m_menuScript.Version.text = "v" + Application.version;
            m_screenScript.ShowTitleScreen();
            
            AddListenerToButtons();

            StockExchange.OrderSucceeded += OnOrderSucceeded;
            TimeKeeper.WeekChanged += OnWeekChanged;
            TimeKeeper.TimerStopped += OnTimerStopped;
        }

        internal void OnDestroy()
        {
            RemoveListenerFromButtons();

            StockExchange.OrderSucceeded -= OnOrderSucceeded;
            TimeKeeper.WeekChanged -= OnWeekChanged;
            TimeKeeper.TimerStopped -= OnTimerStopped;
        }

        internal void Update()
        {
            if (TimeKeeper.IsTimerEnabled)
            {
                TimeKeeper.Update();
                UpdateTimebar(TimeKeeper.WeekProgress);
            }
        }

        #endregion

        private ScreenScript m_screenScript = null!;
        private MenuScript m_menuScript = null!;

        private string m_selectedStockSymbol = "";

        private Color m_selectedBGColor = Color.white;
        private Color m_unselectedBGColor = Color.white;

        private void StartGame()
        {
            StockExchange.ResetStocks();
            StockExchange.ResetCash();

            m_selectedStockSymbol = "";
            ChangeStockBGColor(m_selectedStockSymbol);

            RefreshUIMenu();
            RefreshUIStocks();

            TimeKeeper.StartTimer();
            m_screenScript.ShowGameScreen();
        }

        private void EndGame()
        {
            var result = ResultUmpire.GetResultFromScore(StockExchange.AssetsRate);
            m_menuScript.ResultScore.text = "Score: " + (StockExchange.AssetsRate * 10000f).ToString("N0");
            m_menuScript.ResultRank.text = "Rank: " + result.Rank;
            m_menuScript.ResultMessage.text = result.Message;

            m_screenScript.ShowResultScreen();
        }

        private void BackToTitle()
        {
            TimeKeeper.StopTimer();
            m_screenScript.ShowTitleScreen();   
        }

        // FIXME: StockViewModel にも同じようなフォーマット処理があるのでまとめる
        private void RefreshUIMenu()
        {
            var assetsRate = StockExchange.AssetsRate;
            var rateString = (assetsRate * 100f).ToString("N2");

            if (assetsRate > 0)
            {
                rateString = "+" + rateString;
            }

            // UI を更新する
            m_menuScript.Assets.text = "$ " + StockExchange.Assets.ToString("N0");
            m_menuScript.AssetsRate.text = rateString + " %";
            m_menuScript.Cash.text = "$ " + StockExchange.Cash.ToString("N0");
        }

        private void RefreshUIStocks()
        {
            foreach (var (tickerSymbol, uiStock) in UIStocks)
            {
                var viewModel = StockExchange.StockViewModels[tickerSymbol];

                uiStock.HoldAmount.text = viewModel.AmountString;
                uiStock.HoldAverage.text = viewModel.AverageString;
                uiStock.Price.text = viewModel.PriceString;
                uiStock.PriceRate.text = viewModel.PriceRateString;
            }
        }

        private void UpdateTimebar(float progress)
        {
            m_menuScript.Timebar.fillAmount = 1 - progress;
        }

        #region イベント発火時処理

        private void OnOrderSucceeded()
        {
            RefreshUIMenu();
            RefreshUIStocks();
        }

        private void OnWeekChanged(int weekNumber)
        {
            if (TimeKeeper.IsWeekMoving)
            {
                m_menuScript.WeekNumber.text = $"2022-W{weekNumber}";
            }
            else
            {
                m_menuScript.WeekNumber.text = $"READY... ({TimeKeeper.TakeOffCountdown})";
            }
            
            StockExchange.ChangeWeek(weekNumber);

            RefreshUIMenu();
            RefreshUIStocks();
        }

        private void OnTimerStopped()
        {
            EndGame();
        }

        #endregion

        #region ボタン処理

        private void AddListenerToButtons()
        {
            m_menuScript.Start.onClick.AddListener(OnStartClicked);
            m_menuScript.Restart.onClick.AddListener(OnRestartClicked);
            m_menuScript.Title.onClick.AddListener(OnTitleClicked);

            m_menuScript.Buy1.onClick.AddListener(() => OnBuyStockXClicked(1));
            m_menuScript.Buy10.onClick.AddListener(() => OnBuyStockXClicked(10));
            m_menuScript.Buy100.onClick.AddListener(() => OnBuyStockXClicked(100));
            m_menuScript.BuyMax.onClick.AddListener(OnBuyStockMaxClicked);

            m_menuScript.Sell1.onClick.AddListener(() => OnSellStockXClicked(1));
            m_menuScript.Sell10.onClick.AddListener(() => OnSellStockXClicked(10));
            m_menuScript.Sell100.onClick.AddListener(() => OnSellStockXClicked(100));
            m_menuScript.SellAll.onClick.AddListener(OnSellStockAllClicked);

            foreach (var (tickerSymbol, uiStock) in UIStocks)
            {
                uiStock.Clicked += OnStockClicked;
            }
        }

        private void RemoveListenerFromButtons()
        {
            m_menuScript.Start.onClick.RemoveListener(OnStartClicked);
            m_menuScript.Restart.onClick.RemoveListener(OnRestartClicked);
            m_menuScript.Title.onClick.RemoveListener(OnTitleClicked);

            m_menuScript.Buy1.onClick.RemoveListener(() => OnBuyStockXClicked(1));
            m_menuScript.Buy10.onClick.RemoveListener(() => OnBuyStockXClicked(10));
            m_menuScript.Buy100.onClick.RemoveListener(() => OnBuyStockXClicked(100));
            m_menuScript.BuyMax.onClick.RemoveListener(OnBuyStockMaxClicked);

            m_menuScript.Sell1.onClick.RemoveListener(() => OnSellStockXClicked(1));
            m_menuScript.Sell10.onClick.RemoveListener(() => OnSellStockXClicked(10));
            m_menuScript.Sell100.onClick.RemoveListener(() => OnSellStockXClicked(100));
            m_menuScript.SellAll.onClick.RemoveListener(OnSellStockAllClicked);

            foreach (var (tickerSymbol, uiStock) in UIStocks)
            {
                uiStock.Clicked -= OnStockClicked;
            }
        }

        private void OnStartClicked()
        {
            StartGame();
        }

        private void OnRestartClicked()
        {
            StartGame();
        }

        private void OnTitleClicked()
        {
            BackToTitle();
        }

        private void OnBuyStockXClicked(int amount)
        {
            StockExchange.BuyStock(m_selectedStockSymbol, amount);
        }

        private void OnBuyStockMaxClicked()
        {
            StockExchange.BuyStockMax(m_selectedStockSymbol);
        }

        private void OnSellStockXClicked(int amount)
        {
            StockExchange.SellStock(m_selectedStockSymbol, amount);
        }

        private void OnSellStockAllClicked()
        {
            StockExchange.SellStockAll(m_selectedStockSymbol);
        }

        private void OnStockClicked(string tickerSymbol)
        {
            m_selectedStockSymbol = tickerSymbol;
            ChangeStockBGColor(tickerSymbol);
        }

        private void ChangeStockBGColor(string tickerSymbol)
        {
            foreach (var (ts, stockScript) in UIStocks)
            {
                if (ts == tickerSymbol)
                {
                    stockScript.IconBackground.color = m_selectedBGColor;
                }
                else
                {
                    stockScript.IconBackground.color = m_unselectedBGColor;
                }
            }
        }

        #endregion
    }
}
