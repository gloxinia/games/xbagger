#nullable enable

using System.Collections.Generic;
using Gloxinia.Xbagger.Stock;
using Gloxinia.Xbagger.UI;
using UnityEngine;

namespace Gloxinia.Xbagger.Core
{
    /// <summary>
    /// ゲームの全体的な流れを管理するクラス
    /// GameDirector/StockExchanger/TimeKeeper の使用および連携を行う
    /// 最初に GameDirector への登録を行ったあとはすべて GameDirector 越しに処理を行う
    /// </summary>
    public class GameDirectorScript : MonoBehaviour
    {
        #region インスペクタ公開変数

        [SerializeField]
        private ScreenScript m_screenScript = null!;

        [SerializeField]
        private MenuScript m_menuScript = null!;

        [SerializeField]
        private List<StockProfile> m_stockProfileList = new();

        [SerializeField]
        private List<StockScript> m_stockScriptList = new();

        [SerializeField]
        private int m_weekLength = 52;

        [SerializeField]
        private int m_firstMoney = 10000;

        [SerializeField]
        private Color m_selectedStockBGColor = Color.green;

        [SerializeField]
        private Color m_unselectedStockBGColor = Color.white;

        #endregion

        private GameDirector m_gameDirector = null!;

        private void Awake()
        {
            Init();
            m_gameDirector.Awake();
        }

        private void OnDestroy()
        {
            m_gameDirector.OnDestroy();
        }

        private void Update()
        {
            m_gameDirector.Update();
        }

        /// <summary>
        /// ゲーム初期化処理
        /// 起動時に1回だけ呼ぶこと
        /// </summary>
        private void Init()
        {
            // GameDirector を生成する
            m_gameDirector = new GameDirector(m_weekLength);

            // 初期情報を登録する
            m_gameDirector.RegisterUIScreen(m_screenScript);
            m_gameDirector.RegisterUIMenu(m_menuScript);
            m_gameDirector.RegisterUIStocks(m_stockProfileList, m_stockScriptList);
            m_gameDirector.RegisterBGColors(m_selectedStockBGColor, m_unselectedStockBGColor);

            m_gameDirector.StockExchange.RegisterStocks(m_stockProfileList);
            m_gameDirector.StockExchange.SetFirstMoney(m_firstMoney);
        }
    }
}
