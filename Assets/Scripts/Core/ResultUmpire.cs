#nullable enable

namespace Gloxinia.Xbagger.Core
{
    /// <summary>
    /// 判定の構造体
    /// </summary>
    internal struct Result
    {
        internal string Rank;
        internal string Message;

        internal Result(string rank, string message)
        {
            Rank = rank;
            Message = message;
        }
    }

    /// <summary>
    /// ゲーム終了時のランク判定を行うクラス
    /// </summary>
    internal static class ResultUmpire
    {
        internal static Result GetResultFromScore(float assetsRate)
        {
            if (32f < assetsRate)
            {
                // x33 ~
                return new Result("SSS", "Are you Mr. Elon Musk?");
            }
            else if (16f < assetsRate)
            {
                // x17 ~ x33
                return new Result("SS", "Awesome!!");
            }
            else if (8f < assetsRate)
            {
                // x9 ~ x17
                return new Result("SS", "Excellent!!");
            }
            else if (4f < assetsRate)
            {
                // x5 ~ x9
                return new Result("S", "Excellent!!");
            }
            else if (2f < assetsRate)
            {
                // x3 ~ x5
                return new Result("A", "Great!");
            }
            else if (1f < assetsRate)
            {
                // x2 ~ x3
                return new Result("B", "Good!");
            }
            else if (0f < assetsRate)
            {
                // x1 ~ x2
                return new Result("C", "Make more money!");
            }
            else if (-0.5f < assetsRate)
            {
                // x0.5 ~ x1
                return new Result("D", "Let's try it again.");
            }
            else if (-0.75f < assetsRate)
            {
                //  x0.25 ~ x0.5
                return new Result("E", "You are lucky this is a game.");
            }
            else
            {
                //  ~ x0.25
                return new Result("F", "Do not touch real world stocks.");
            }
        }
    }
}
