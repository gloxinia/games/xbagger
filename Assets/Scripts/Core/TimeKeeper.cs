#nullable enable

using System;
using UnityEngine;

namespace Gloxinia.Xbagger.Core
{
    /// <summary>
    /// ゲームの時間経過を管理するクラス
    /// </summary>
    internal class TimeKeeper
    {
        #region Internal API

        /// <summary>
        /// 現在タイマーが有効か
        /// </summary>
        internal bool IsTimerEnabled { get; private set; } = false;

        /// <summary>
        /// 現在週が移り変わっているか
        /// </summary>
        internal bool IsWeekMoving { get; private set; } = false;

        /// <summary>
        /// 週番号
        /// </summary>
        internal int WeekNumber { get; private set; } = 0;

        /// <summary>
        /// 週が移り変わる前の準備中フェーズのカウントダウン
        /// </summary>
        internal int TakeOffCountdown => Mathf.CeilToInt(m_takeOffWeeks - m_gameTimer / m_weekSeconds);

        /// <summary>
        /// 週の進行度
        /// 週が始まった瞬間が0 週が終わる瞬間が1
        /// </summary>
        internal float WeekProgress => m_weekTimer / m_weekSeconds;

        /// <summary>
        /// 週が移り変わったときに発火するイベント
        /// </summary>
        internal event Action<int>? WeekChanged;

        /// <summary>
        /// ゲームが終了したときに発火するイベント
        /// </summary>
        internal event Action? TimerStopped;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        internal TimeKeeper(int limitWeeks)
        {
            m_limitWeeks = limitWeeks;
        }

        /// <summary>
        /// タイマーを起動する
        /// </summary>
        internal void StartTimer()
        {
            IsTimerEnabled = true;
            IsWeekMoving = false;
            WeekNumber = 0;
            m_gameTimer = 0f;
            m_weekTimer = 0f;

            // 最初の1回だけここで発火する
            WeekChanged?.Invoke(WeekNumber);
            Debug.Log($"[TimeKeeper] Timer is started.");
        }

        /// <summary>
        /// タイマーを停止する
        /// </summary>
        internal void StopTimer()
        {
            IsTimerEnabled = false;
            m_gameTimer = 0f;
            m_weekTimer = 0f;

            // ゲームが終了したイベントを発火する
            TimerStopped?.Invoke();
            Debug.Log("[TimeKeeper] Timer is stopped.");
        }

        /// <summary>
        /// MonoBehaviour.Update() 内で実行する
        /// </summary>
        internal void Update()
        {
            // タイマーが有効でない場合 何もしない
            if (IsTimerEnabled == false) return;

            // ゲームタイマーおよび週タイマーを進める
            m_gameTimer += Time.deltaTime;
            m_weekTimer += Time.deltaTime;

            // ゲームタイマーが準備中を超えている場合
            if (m_weekSeconds * m_takeOffWeeks < m_gameTimer)
            {
                // まだフラグが切り替わっていない場合
                if (IsWeekMoving == false)
                {
                    // フラグを切り替える
                    // このブロックは一度だけ通過する
                    IsWeekMoving = true;
                    WeekNumber++;
                }
            }

            // 週タイマーが1週当たりの秒数を超えている場合
            if (m_weekTimer > m_weekSeconds)
            {
                // 週タイマーをリセットする
                m_weekTimer = 0f;
                WeekChanged?.Invoke(WeekNumber);

                // 週が移り変わっている場合
                if (IsWeekMoving)
                {
                    // 週が移り変わったイベントを発火する
                    WeekNumber++;
                    Debug.Log($"[TimeKeeper] Week is changed to W-{WeekNumber}.");
                }
            }

            // ゲームタイマーがゲームの制限時間を超えている場合
            if (m_gameTimer > m_weekSeconds * (m_takeOffWeeks + m_limitWeeks))
            {
                // タイマーを停止する
                StopTimer();
            }
        }

        #endregion

        private int m_limitWeeks = 0;
        private float m_gameTimer = 0f;
        private float m_weekTimer = 0f;
        private readonly int m_takeOffWeeks = 3;
        private readonly float m_weekSeconds = 1f;
    }
}
