#nullable enable

using UnityEngine;
using Gloxinia.Xbagger.Stock.Price;

namespace Gloxinia.Xbagger.Stock
{
    /// <summary>
    /// 証券所で取引されている株式を表現するクラス
    /// </summary>
    public class DealingStock
    {
        #region Internal API

        internal float FirstPrice { get; private set; } = 1f;

        internal float CurrentPrice { get; private set; } = 1f;

        internal float PriceRate => CurrentPrice / FirstPrice - 1f;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        internal DealingStock(int limitWeeks, StockProfile stockProfile)
        {
            m_limitWeeks = limitWeeks;
            m_stockProfile = stockProfile;
        }

        internal void Reset()
        {
            // 株価の初期値を設定する
            FirstPrice = GetFirstPrice();
            CurrentPrice = FirstPrice;

            // 株価の値動きを取得する
            var priceMovement = new PriceMovementCollatz
            (
                firstPrice: FirstPrice,
                salt: Random.Range(1000, 10000),
                stretchRatioMin: m_stretchRatioBase,
                stretchRatioMax: Mathf.CeilToInt(m_stretchRatioBase * m_stockProfile.PriceMovementFluency),
                randomizeRatioRange: m_randomizeRatioBase * m_stockProfile.PriceMovementIntensity
            );

            m_priceList = (priceMovement as IPriceMovement).GetPriceList(m_limitWeeks + 1);
        }

        internal void ChangeWeek(int weekNumber)
        {
            CurrentPrice = m_priceList[weekNumber];
        }

        #endregion

        private int m_limitWeeks = 0;
        private StockProfile m_stockProfile = null!;

        private float[] m_priceList = {};
        private readonly int m_stretchRatioBase = 5;
        private readonly float m_randomizeRatioBase = 0.1f;

        private float GetFirstPrice()
        {
            return Random.Range(m_stockProfile.FirstPriceRangeMin, m_stockProfile.FirstPriceRangeMax);
        }

    }
}
