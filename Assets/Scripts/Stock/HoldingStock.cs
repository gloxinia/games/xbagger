#nullable enable

namespace Gloxinia.Xbagger.Stock
{
    /// <summary>
    /// プレイヤーが所有している株式を表現するクラス
    /// </summary>
    public class HoldingStock
    {
        #region Internal API

        internal int Amount { get; private set; } = 0;

        internal float Average { get; private set; } = 0f;

        internal float Value => m_dealingStock.CurrentPrice * Amount;

        internal HoldingStock(DealingStock dealingStock)
        {
            m_dealingStock = dealingStock;
        }

        internal void Reset()
        {
            Amount = 0;
            Average = 0f;
        }

        internal void Buy(int amount)
        {
            var currentValue = Average * Amount;
            Amount += amount;
            Average = (currentValue + m_dealingStock.CurrentPrice * amount) / Amount;
        }

        internal void Sell(int amount)
        {
            Amount -= amount;
        }

        #endregion 

        private DealingStock m_dealingStock = null!;
    }
}
