#nullable enable

using UnityEngine;

namespace Gloxinia.Xbagger.Stock
{
    /// <summary>
    /// 株式の特徴を持つ ScriptableObject
    /// </summary>
    [CreateAssetMenu(fileName = "New StockData", menuName = "XBAGGER/New StockData")]
    public class StockProfile : ScriptableObject
    {
        /// <summary>
        /// 銘柄のティッカーシンボル
        /// </summary>
        public string TickerSymbol => m_tickerSymbol;
        [SerializeField]
        private string m_tickerSymbol = "";

        /// <summary>
        /// 企業のアイコン画像
        /// </summary>
        public Sprite CompanyIcon => m_companyIconSprite;
        [SerializeField]
        private Sprite m_companyIconSprite = null!;

        /// <summary>
        /// ランダムに決まる株価の初期値の範囲 (最小値)
        /// </summary>
        internal float FirstPriceRangeMin => m_firstPriceRangeMin;
        [SerializeField]
        private float m_firstPriceRangeMin = 0f;

        /// <summary>
        /// ランダムに決まる株価の初期値の範囲 (最大値)
        /// </summary>
        internal float FirstPriceRangeMax => m_firstPriceRangeMax;
        [SerializeField]
        private float m_firstPriceRangeMax = 1000f;

        /// <summary>
        /// 株価の値動きの種類
        /// </summary>
        internal PriceMovementType PriceMovementType => m_priceMovementType;
        [SerializeField]
        private PriceMovementType m_priceMovementType = PriceMovementType.None;

        /// <summary>
        /// 株価の値動きの激しさ 1が標準
        /// 何を意味するかは値動きの種類によって変わる
        /// </summary>
        internal float PriceMovementIntensity => m_priceMovementIntensity;
        [SerializeField]
        private float m_priceMovementIntensity = 1f;

        /// <summary>
        /// 株価の値動きのなだらかさ 1が標準
        /// 何を意味するかは値動きの種類によって変わる
        /// </summary>
        internal float PriceMovementFluency => m_priceMovementFluency;
        [SerializeField]
        private float m_priceMovementFluency = 1f;
    }
}
