namespace Gloxinia.Xbagger.Stock.Price
{
    /// <summary>
    /// 株価の値動きのための Interface
    /// </summary>
    public interface IPriceMovement
    {
        /// <summary>
        /// 株価の値動きを取得する
        /// </summary>
        /// <param name="length">株価の値動きの配列の長さ</param>
        /// <returns>株価の値動き</returns>
        public float[] GetPriceList(int length);
    }
}
