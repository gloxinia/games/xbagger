#nullable enable

namespace Gloxinia.Xbagger.Stock.Price
{
    /// <summary>
    /// 株式の値動きに関する共通処理をまとめた抽象クラス
    /// </summary>
    internal abstract class BasePriceMovement
    {
        /// <summary>
        /// 数値を指定の範囲内で引き伸ばす
        /// 例: StretchValue(0, 2, 3) => [0f, 0.66f, 1.32f]
        /// </summary>
        /// <param name="from">範囲の開始値</param>
        /// <param name="to">範囲の終了値</param>
        /// <param name="stretchRatio">何個分に引き伸ばすか</param>
        /// <returns>引き伸ばされた値の配列</returns>
        protected float[] StretchValue(float from, float to, int stretchRatio)
        {
            var list = new float[stretchRatio];

            for (int i = 0; i < stretchRatio; i++)
            {
                var stretchedValue = from + (to - from) / stretchRatio * i;
                list[i] = stretchedValue;
            }

            return list;
        }

        /// <summary>
        /// 数値を指定の範囲内でブレさせる
        /// </summary>
        /// <param name="value">指定の数値</param>
        /// <param name="randomizeRatioRange">ブレの範囲 (ランダム率)</param>
        /// <returns>ブレさせられた数値</returns>
        protected float RandomizeValue(float value, float randomizeRatioRange)
        {
            var randomizeRatioMin = 1f - randomizeRatioRange;
            var randomizeRatioMax = 1f + randomizeRatioRange;
            var randomizeRatio = UnityEngine.Random.Range(randomizeRatioMin, randomizeRatioMax);

            return value * randomizeRatio;
        }
    }
}
