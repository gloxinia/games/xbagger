#nullable enable

namespace Gloxinia.Xbagger.Stock.Price
{
    /// <summary>
    /// コラッツの問題に基づく株価の値動きのクラス
    /// ref. https://ja.wikipedia.org/wiki/%E3%82%B3%E3%83%A9%E3%83%83%E3%83%84%E3%81%AE%E5%95%8F%E9%A1%8C
    /// </summary>
    internal class PriceMovementCollatz : BasePriceMovement, IPriceMovement
    {
        #region IPriceMovement implements

        float[] IPriceMovement.GetPriceList(int length)
        {
            var list = new float[length];
            list[0] = m_firstPrice;

            var prev = m_salt;

            for (int weekCount = 1; weekCount < length; weekCount++)
            {
                var next = ApplyCollatzRule(prev);
                var stratchRatio = UnityEngine.Random.Range(m_stretchRatioMin, m_stretchRatioMax);
                var stretchedValues = StretchValue(prev, next, stratchRatio);

                for (int stretchCount = 0; stretchCount < stretchedValues.Length; stretchCount++)
                {
                    var randomizedValue = RandomizeValue(stretchedValues[stretchCount], m_randomizeRatioRange);
                    var rate = (float)randomizedValue / m_salt;
                    var price = UnityEngine.Mathf.CeilToInt(m_firstPrice * rate);
                    list[weekCount] = price;

                    // 最終ループ以外は週カウンターを1進める
                    // 最終ループは for ループでカウンターが1進む
                    if (stretchCount != stretchedValues.Length - 1)
                    {
                        weekCount++;
                    }

                    // 週カウンターが最大値に達している場合はループを抜ける
                    if (weekCount > length - 1) break;
                }

                prev = next;
            }

            return list;
        }

        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="weekCount">値動きの配列の長さ</param>
        /// <param name="firstPrice">価格の初期値</param>
        /// <param name="salt">値動きに使用するソルト コラッツ数列に使用する</param>
        /// <param name="stretchRatioMin">引き伸ばし率の最小値</param>
        /// <param name="stretchRatioMax">引き伸ばし率の最大値</param>
        /// <param name="randomizeRatioRange">ランダム率</param>
        internal PriceMovementCollatz
        (
            float firstPrice,
            int salt,
            int stretchRatioMin,
            int stretchRatioMax,
            float randomizeRatioRange
        )
        {
            m_firstPrice = firstPrice;
            m_salt = salt;
            m_stretchRatioMin = stretchRatioMin;
            m_stretchRatioMax = stretchRatioMax;
            m_randomizeRatioRange = randomizeRatioRange;
        }


        /// <summary>
        /// 指定の数値に対してコラッツの問題に基づく操作を行う
        /// </summary>
        /// <returns>操作を行ったあとの数値</returns>
        private int ApplyCollatzRule(int n)
        {
            if (n % 2 == 0)
            {
                // n が偶数の場合は n を 2 で割る
                return n / 2;
            }
            else
            {
                // n が奇数の場合は n に 3 をかけて 1 を足す
                return n * 3 + 1;
            }
        }

        private float m_firstPrice = 0;
        private int m_salt = 0;
        private int m_stretchRatioMin = 0;
        private int m_stretchRatioMax = 0;
        private float m_randomizeRatioRange = 0f;
    }
}
