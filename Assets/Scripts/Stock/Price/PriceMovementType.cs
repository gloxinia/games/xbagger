namespace Gloxinia.Xbagger.Stock
{
    /// <summary>
    /// 株価の値動きの種類
    /// </summary>
    public enum PriceMovementType
    {
        None,
        Wave,
        Collatz,
    }
}
