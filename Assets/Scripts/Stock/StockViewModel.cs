#nullable enable

namespace Gloxinia.Xbagger.Stock
{
    /// <summary>
    /// 株式の UI 表示用のデータと処理をまとめたクラス
    /// </summary>
    public class StockViewModel
    {
        #region Public API

        public float Price => m_dealingStock.CurrentPrice;

        public string PriceString => FormatPrice(Price);

        public float PriceRate => m_dealingStock.PriceRate;

        public string PriceRateString => FormatRate(PriceRate);

        public int Amount => m_holdingStock.Amount;

        public string AmountString => "x " + Amount.ToString("N0");

        public float Average => m_holdingStock.Average;

        public string AverageString => "@ $ " + Average.ToString("N2");

        public StockViewModel(DealingStock dealingStock, HoldingStock holdingStock)
        {
            m_dealingStock = dealingStock;
            m_holdingStock = holdingStock;
        }

        #endregion

        private DealingStock m_dealingStock = null!;
        private HoldingStock m_holdingStock = null!;

        private string FormatPrice(float price)
        {
            return "$ " + price.ToString("N0");
        }

        private string FormatRate(float rate)
        {
            var rateString = (rate * 100f).ToString("N2");

            if (rate > 0)
            {
                rateString = "+" + rateString;
            }

            return rateString + " %";
        }
    }
}
