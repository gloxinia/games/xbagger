#nullable enable

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gloxinia.Xbagger.Stock
{
    /// <summary>
    /// 株式の取引を受け持つクラス
    /// GameDirectorScript の Awake() 内で作成される
    /// 総資産 = 所持金 + 所持株式の評価額
    /// </summary>
    public class StockExchange
    {
        #region Public API

        /// <summary>
        /// 取扱株式のリスト
        /// 辞書の key はティッカーシンボル
        /// </summary>
        public Dictionary<string, DealingStock> DealingStocks { get; private set; } = new();

        /// <summary>
        /// 所有株式のリスト
        /// 辞書の key はティッカーシンボル
        /// </summary>
        public Dictionary<string, HoldingStock> HoldingStocks { get; private set; } = new();

        /// <summary>
        /// 株式の取扱情報と所有情報を UI 向けにまとめた ViewModel のリスト
        /// 辞書の key はティッカーシンボル
        /// </summary>
        public Dictionary<string, StockViewModel> StockViewModels { get; private set; } = new();

        /// <summary>
        /// 現在の総資産 (= 現金 + 評価額)
        /// </summary>
        public float Assets => Cash + GetTotalValue();

        /// <summary>
        /// ゲーム開始時からの総資産の変動率
        /// ゲームのスコアでもある
        /// </summary>
        public float AssetsRate => Assets / m_firstMoney - 1f;

        /// <summary>
        /// 現在の現金
        /// </summary>
        public float Cash { get; private set; } = 0;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public StockExchange(int limitWeeks)
        {
            m_limitWeeks = limitWeeks;
        }

        /// <summary>
        /// 株式の売買が成功したときに発火するイベント
        /// </summary>
        public event Action? OrderSucceeded;

        /// <summary>
        /// 株式の売買が失敗したときに発火するイベント
        /// </summary>
        public event Action? OrderFailed;

        /// <summary>
        /// 取引株式および所持株式を設定する
        /// </summary>
        public void RegisterStocks(List<StockProfile> stockProfileList)
        {
            DealingStocks = new();
            HoldingStocks = new();
            StockViewModels = new();

            foreach (var stockProfile in stockProfileList)
            {
                var tickerSymbol = stockProfile.TickerSymbol;
                var dealingStock = new DealingStock(m_limitWeeks, stockProfile);
                var holdingStock = new HoldingStock(dealingStock);
                var stockViewModel = new StockViewModel(dealingStock, holdingStock);

                DealingStocks[tickerSymbol] = dealingStock;
                HoldingStocks[tickerSymbol] = holdingStock;
                StockViewModels[tickerSymbol] = stockViewModel;

                Debug.Log($"[StockExchange] Complete to register stock: {tickerSymbol}.");
            }
        }

        /// <summary>
        /// 所持現金の初期値を設定する
        /// </summary>
        public void SetFirstMoney(float firstMoney)
        {
            m_firstMoney = firstMoney;
        }

        /// <summary>
        /// 取引株式および所持株式をリセットする
        /// </summary>
        public void ResetStocks()
        {
            foreach (var tickerSymbol in DealingStocks.Keys)
            {
                var dealingStock = DealingStocks[tickerSymbol];
                var holdingStock = HoldingStocks[tickerSymbol];

                dealingStock.Reset();
                holdingStock.Reset();
            }
        }

        /// <summary>
        /// 所持現金をリセットする
        /// </summary>
        public void ResetCash()
        {
            Cash = m_firstMoney;
        }

        /// <summary>
        /// 週を変更する
        /// </summary>
        public void ChangeWeek(int weekNumber)
        {
            foreach (var (tickerSymbol, dealingStock) in DealingStocks)
            {
                dealingStock.ChangeWeek(weekNumber);
            }
        }

        /// <summary>
        /// 株式を購入する
        /// </summary>
        public bool BuyStock(string tickerSymbol, int amount)
        {
            // ティッカーシンボルが空文字の場合
            if (tickerSymbol == "")
            {
                // 購入に失敗する
                OrderFailed?.Invoke();
                Debug.Log($"[StockExchange] Order is failed. Select a stock.");
                return false;
            }

            var dealingStock = DealingStocks[tickerSymbol];
            var holdingStock = HoldingStocks[tickerSymbol];
            var buyMoney = dealingStock.CurrentPrice * amount;

            // お金が足りない場合 
            if (Cash < buyMoney)
            {
                // 購入に失敗する
                OrderFailed?.Invoke();
                Debug.Log($"[StockExchange] Order (buy {tickerSymbol} x {amount}) is failed.");
                return false;
            }
            // お金が足りている場合
            else
            {
                // 現金を減らす
                Cash -= buyMoney;

                // 購入に成功する
                holdingStock.Buy(amount);

                OrderSucceeded?.Invoke();
                Debug.Log($"[StockExchange] Order (buy {tickerSymbol} x {amount}) is succeeded.");
                return true;
            }
        }

        /// <summary>
        /// 株式を買えるだけ購入する
        /// </summary>
        public bool BuyStockMax(string tickerSymbol)
        {
            var dealingStock = DealingStocks[tickerSymbol];
            var amount = Mathf.FloorToInt(Cash / dealingStock.CurrentPrice);

            return BuyStock(tickerSymbol, amount);
        }

        /// <summary>
        /// 株式を売却する
        /// </summary>
        public bool SellStock(string tickerSymbol, int amount)
        {
            // ティッカーシンボルが空文字の場合
            if (tickerSymbol == "")
            {
                // 購入に失敗する
                OrderFailed?.Invoke();
                Debug.Log($"[StockExchange] Order is failed. Select a stock.");
                return false;
            }

            var dealingStock = DealingStocks[tickerSymbol];
            var holdingStock = HoldingStocks[tickerSymbol];

            // 所有数が足りていない場合
            if (holdingStock.Amount < amount)
            {
                // 売却に失敗する
                OrderFailed?.Invoke();
                Debug.Log($"[StockExchange] Order (sell {tickerSymbol} x {amount}) is failed.");
                return false;
            }
            // 所有数が足りている場合
            else
            {
                // 現金を増やす
                var sellMoney = dealingStock.CurrentPrice * amount;
                Cash += sellMoney;

                // 売却に成功する
                holdingStock.Sell(amount);

                OrderSucceeded?.Invoke();
                Debug.Log($"[StockExchange] Order (sell {tickerSymbol} x {amount}) is succeeded.");
                return true;
            }
        }

        /// <summary>
        /// 株式をすべて売却する
        /// </summary>
        public bool SellStockAll(string tickerSymbol)
        {
            var holdingStock = HoldingStocks[tickerSymbol];

            return SellStock(tickerSymbol, holdingStock.Amount);
        }

        #endregion

        private int m_limitWeeks = 0;
        private float m_firstMoney = 0f;

        private float GetTotalValue()
        {
            var value = 0f;

            foreach (var (tickerSymbol, holdingStock) in HoldingStocks)
            {
                value += holdingStock.Value;
            }

            return value;
        }
    }
}
