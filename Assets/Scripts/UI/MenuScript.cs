#nullable enable

using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gloxinia.Xbagger.UI
{
    /// <summary>
    /// UI におけるメニューのボタンやテキストを取り扱うクラス
    /// </summary>
    public class MenuScript : MonoBehaviour
    {
        public Button Start => m_start;
        [Header("Outgame Buttons")]
        [SerializeField]
        private Button m_start = null!;

        public Button Restart => m_restart;
        [SerializeField]
        private Button m_restart = null!;

        public TMP_Text Version => m_version;
        [Header("Outgame Texts")]
        [SerializeField]
        private TMP_Text m_version = null!;

        public TMP_Text ResultScore => m_resultScore;
        [SerializeField]
        private TMP_Text m_resultScore = null!;

        public TMP_Text ResultRank => m_resultRank;
        [SerializeField]
        private TMP_Text m_resultRank = null!;

        public TMP_Text ResultMessage => m_resultMessage;
        [SerializeField]
        private TMP_Text m_resultMessage = null!;

        public Button Title => m_title;
        [Header("Ingame Buttons")]
        [SerializeField]
        private Button m_title = null!;

        public Button Buy1 => m_buy1;
        [SerializeField]
        private Button m_buy1 = null!;

        public Button Buy10 => m_buy10;
        [SerializeField]
        private Button m_buy10 = null!;

        public Button Buy100 => m_buy100;
        [SerializeField]
        private Button m_buy100 = null!;

        public Button BuyMax => m_buyMax;
        [SerializeField]
        private Button m_buyMax = null!;

        public Button Sell1 => m_sell1;
        [SerializeField]
        private Button m_sell1 = null!;

        public Button Sell10 => m_sell10;
        [SerializeField]
        private Button m_sell10 = null!;

        public Button Sell100 => m_sell100;
        [SerializeField]
        private Button m_sell100 = null!;

        public Button SellAll => m_sellAll;
        [SerializeField]
        private Button m_sellAll = null!;

        public TMP_Text WeekNumber => m_weekNumber;
        [Header("Ingame Texts")]
        [SerializeField]
        private TMP_Text m_weekNumber = null!;

        public TMP_Text Assets => m_assets;
        [SerializeField]
        private TMP_Text m_assets = null!;

        public TMP_Text AssetsRate => m_assetsRate;
        [SerializeField]
        private TMP_Text m_assetsRate = null!;

        public TMP_Text Cash => m_cash;
        [SerializeField]
        private TMP_Text m_cash = null!;

        public Image Timebar => m_timebar;
        [Header("Images")]
        [SerializeField]
        private Image m_timebar = null!;
    }
}
