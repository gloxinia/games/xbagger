#nullable enable

using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Gloxinia.Xbagger.UI
{
    /// <summary>
    /// UI における株式1つあたりの領域を取り扱うクラス
    /// </summary>
    public class StockScript : MonoBehaviour, IPointerClickHandler
    {
        #region IPointerClickHandler implements

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            Clicked?.Invoke(TickerSymbol);
            Debug.Log($"[Stock:{TickerSymbol}] Clicked.");
        }

        #endregion

        public string TickerSymbol { get; set; } = "";

        public Image Icon => m_icon;
        [SerializeField]
        private Image m_icon = null!;

        public Image IconBackground => m_iconBackground;
        [SerializeField]
        private Image m_iconBackground = null!;

        public TMP_Text Name => m_textName;
        [SerializeField]
        private TMP_Text m_textName = null!;

        public TMP_Text Price => m_textPrice;
        [SerializeField]
        private TMP_Text m_textPrice = null!;

        public TMP_Text PriceRate => m_textPriceRate;
        [SerializeField]
        private TMP_Text m_textPriceRate = null!;

        public TMP_Text HoldAmount => m_textHoldAmount;
        [SerializeField]
        private TMP_Text m_textHoldAmount = null!;

        public TMP_Text HoldAverage => m_textHoldAverage;
        [SerializeField]
        private TMP_Text m_textHoldAverage = null!;

        /// <summary>
        /// 株式全体がクリックされたときに発火するイベント
        /// </summary>
        public event Action<string>? Clicked;
    }
}
