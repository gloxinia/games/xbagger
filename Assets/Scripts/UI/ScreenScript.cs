#nullable enable

using UnityEngine;

namespace Gloxinia.Xbagger.UI
{
    /// <summary>
    /// UI における画面領域を取り扱うクラス
    /// </summary>
    public class ScreenScript : MonoBehaviour
    {
        public void ShowGameScreen()
        {
            m_canvasOutgame.SetActive(false);
        }

        public void ShowTitleScreen()
        {
            m_canvasOutgame.SetActive(true);

            m_screenBodyTitle.SetActive(true);
            m_screenFooterTitle.SetActive(true);

            m_screenBodyResult.SetActive(false);
            m_screenFooterResult.SetActive(false);
        }

        public void ShowResultScreen()
        {
            m_canvasOutgame.SetActive(true);

            m_screenBodyTitle.SetActive(false);
            m_screenFooterTitle.SetActive(false);

            m_screenBodyResult.SetActive(true);
            m_screenFooterResult.SetActive(true);
        }

        [SerializeField]
        private GameObject m_canvasOutgame = null!;

        [SerializeField]
        private GameObject m_screenBodyTitle = null!;

        [SerializeField]
        private GameObject m_screenFooterTitle = null!;

        [SerializeField]
        private GameObject m_screenBodyResult = null!;

        [SerializeField]
        private GameObject m_screenFooterResult = null!;
    }
}
